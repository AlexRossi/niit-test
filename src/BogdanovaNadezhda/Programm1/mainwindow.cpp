#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "circle.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbR_clicked()
{
    QString r=ui->leR->text();
    cir.setRadius(r.toDouble());
    ui->leF->setText(QString::number(cir.getFerence()));
    ui->leA->setText(QString::number(cir.getArea()));
}

void MainWindow::on_pbF_clicked()
{
    QString f=ui->leF->text();
    cir.setFerence(f.toDouble());
    ui->leR->setText(QString::number(cir.getRadius()));
    ui->leA->setText(QString::number(cir.getArea()));
}

void MainWindow::on_pbA_clicked()
{
    QString a=ui->leA->text();
    cir.setArea(a.toDouble());
    ui->leR->setText(QString::number(cir.getRadius()));
    ui->leF->setText(QString::number(cir.getFerence()));
}

void MainWindow::on_pbTask1_clicked()
{

    QString r=ui->lineEdit->text();
    cir.setRadius(N);
    double ference=cir.getFerence()+r.toDouble()/1000;
    cir.setFerence(ference);
    ui->lineEdit_2->setText(QString::number((cir.getRadius()-N)*1000));
}

void MainWindow::on_pbTask2_clicked()
{
    QString radius=ui->leRadius->text();
    QString width=ui->leWidth->text();
    cir.setRadius(radius.toDouble());
    double ference=cir.getFerence();
    double area=cir.getArea();
    cir.setRadius(radius.toDouble()+width.toDouble());
    area=cir.getArea()-area;
    ui->leResult->setText(QString::number(ceil(area)*1000+ceil(ference)*2000));
}
